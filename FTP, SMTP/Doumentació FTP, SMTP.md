# **Documentació FTP**
Instal·lació del servei:

![](Imatges/1.png)

Configuració bàsica perquè demani autenticació:
![](Imatges/2.png)
![](Imatges/3.png)

Comprovació:

![](Imatges/4.png)
# **Documentació Postifx**
Instal·lació del servei:

![](Imatges/5.png)
