# Índex
### Seguiment de projecte

[Condicionants](https://gitlab.com/k3v1n0nc10/projecte-final-seguretat-serveis/blob/master/Llista%20de%20condicionants%20i%20solucions.pdf)

[Mapa lògic](https://gitlab.com/k3v1n0nc10/projecte-final-seguretat-serveis/raw/master/Mapa_l%C3%B2gic)

### Documentació

[Apache](Apache/Apache.md)

[FTP, SMTP](https://gitlab.com/k3v1n0nc10/projecte-final-seguretat-serveis/blob/master/FTP,%20SMTP/Doumentaci%C3%B3%20FTP,%20SMTP.md)

[VPN](VPN/Documentació_VPN.md)

### Manuals
[Manual d'ús d'SSH](https://gitlab.com/k3v1n0nc10/projecte-final-seguretat-serveis/blob/master/Manuals%20d'ús/Manual_SSH.md)

[Manual d'ús TeamViewer](https://gitlab.com/k3v1n0nc10/projecte-final-seguretat-serveis/blob/master/Manuals%20d'ús/Manual_TeamViewer.md)

[Manual d'ús VPN](https://gitlab.com/k3v1n0nc10/projecte-final-seguretat-serveis/blob/master/Manuals%20d'ús/Manual_VPN.md)
