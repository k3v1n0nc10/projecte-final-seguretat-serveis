# **Documentació OpenVPN**
Configuració final del servidor VPN.

![](Imatges/1.png)

![](Imatges/2.png)

![](Imatges/3.png)

![](Imatges/4.png)

![](Imatges/11.png)

![](Imatges/6.png)

Aquí podem veure que ja apareix el nostre servidor.

![](Imatges/7.png)

Després instal·lem el paquet "OpenVPN Client Export" per administrar els clients del servidor.

![](Imatges/8.png)

Creem l'usuari:

![](Imatges/9.png)

Confirmem que podem veure l'usuari en VPN > Open VPN > Client Export

![](Imatges/10.png)

Anem a comprovar que funciona, exportem l'usuari baixant el fitxer "Most Clients", anem a una altre maquina virtual, la fiquem en la mateixa xarxa que el servidor i obrim el fitxer descarregat.

![](Imatges/12.png)

Efectivament podem veure com s'ha creat el tunel.

![](Imatges/13.png)
