# Apache

Hem configurat vostres dues pàgines web per que vagin siguin més segures. Hem creat un certificat SSL i hem configurat que les pàgines vagin per el ports segur 443.

**Web Corporativa:**

![](imgs/web.png) 

**Web de Comerç Electrònic:**

![](imgs/comercio.png) 

**Certificat:**

![](imgs/cert.png) 

Per tal de que les webs puguin ser visibles per a la resta de la classe configurem el gateway de la màquina d'Apache per tal de que surti per el firewall extern, el qual surt a internet, és a dir, a la classe.