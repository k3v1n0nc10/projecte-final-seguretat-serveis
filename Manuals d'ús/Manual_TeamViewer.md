# Manual d'ús TeamViewer

Per poder utilitzar el vostre ordinador de l'empresa desde casa de manera gràfica únicament haureu d'instal·lar TeamViewer als dos equips, al equip del treball i al de casa, o al de una altre empresa, qualsevol que vulguis utilitzar.

Una vegada instal·lat el TeamViewer haurás d'obrir-lo als dos equips, fixat que a cadascún dels TeamViewers hi han un ID i una contrasenya:

![](imgs/teamviewer.png)


A on posa "ID de asociado" has de posar la ID de l'altre equip, al qual et vols connectar. Recorda que si l'altre equip està apagat no podràs connectar-te. Una vegada estableixi connexió et demanarà que ingresis la contrasenya, i una vegada fet aixó ja tindràs disponible l'altre equip a la teva pantalla.

Recorda també que pots cambiar la contrasenya per la que tu vulguis.
