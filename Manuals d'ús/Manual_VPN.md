# Manual d'ús VPN

Hem configurat pfsense per utilitzar OpenVPN i crear una xarxa privada virtual i que els treballadors puguin connectar-se a la xarxa de l'empresa amb els seus portàtils.

Si ets un treballador i no tens clar com pots utilitzar aquest servei t'ho expliquem a aquest document.

###Windows

#### Primera posada en marxa

La primera vegada que vulguis utilitzar l'VPN haurás de iniciar l'instal·lador del programa VPN i el configurador que t'haurém pasat per el correu corporatiu de l'Empresa, recorda que són dos paquets.

![](imgs/openvpninstallers.png) 

Després d'haber instal·lat OpenVPN desde el paquet que us hem distribuit heu d'obrir el vostre client d'OpenVPN i importar el fitxer de configuració ovpn (el arxiu restant). Un cop importat ja podreu connectar-vos especificant l'usuari "openvpn" i la contrasenya "P@ssw0rd", sense les cometes.

#### Connexió

Una vegada instal·lat i configurat cada vegada que vulguis entrar de nou no et demanarà l'usuari ni la password, per tant despres de la primera posada en marxa simplement executa el programa, espera una mica i ja estarás dintre de la xarxa.

###Ubuntu

#### Primera posada en marxa

La primera vegada que vulguis utilitzar l'VPN haurás de descarregar el fitxer de configuració, el qual t'haurem distribuït per el correu de l'empresa.

Després has de accedir a la configuració de xarxa del teu equip Ubuntu.

![](imgs/parametresxarxa.png) 

Hauràs de fer click a l'apartat "VPN", veuràs que tens l'opció d'importar la configuració desde un fitxer, clica a aquella opció i després selecciona l'arxiu descarregat anteriorment.

![](imgs/afegeixvpn.png) 

Quan el arxiu s'obri hauràs de específicar l'usuari "openvpn" i la contrasenya "P@ssw0rd", sense les cometes. I després donar-li a acceptar.

#### Connexió

Una vegada instal·lat i configurat cada vegada que vulguis entrar de nou no et demanarà l'usuari ni la password, per tant despres de la primera posada en marxa simplement torna a anar als paràmetres de xarxa i activa l'VPN nou que et sortirà a la secció, espera una mica i ja estarás dintre de la xarxa.

![](imgs/vpnpuesto.png) 