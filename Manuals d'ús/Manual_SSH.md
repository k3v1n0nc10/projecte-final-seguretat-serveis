# Manual d'ús SSH

En cas de que necessitis accedir a un equip de l'Empresa, com per exempleun servidor, o un equip en el qual únicament pretendis utilitzar la consola no serà necessari que utilitzis TeamViewer. Hem instal·lat SSH a tots els equips de l'empresa per tant podeu utilitzar aquest servei per utilitzar únicament el mode text.

Únicament haureu de escriure al vostre trminal:

	"ssh usuari@ip"

Per exemple, si jo vull connectar-me a l'usuari pedrofernandez del host amb IP 172.16.100.7 escriuria aixó:

![](imgs/ssh.png) 

Per connectar-te necessitarás la contrasenya del usuari al qual t'estàs intentant connectarme. Com podeu veure al executar la comanda em demana la contrasenya de l'usuari "pedrofernandez" i al verificar la contrasenya ja et deixa entrar i et surt un missatge tal que:

![](imgs/ssh2.png) 

En cas de que necessitis executar un programa amb interficie gràfica a l'altre equip hauràs de connectar-te utilitzant el paràmetre -X, tal que:

	"ssh -X usuari@ip"

Per veure mes paràmetres en cas de que ho necesitis pots executar "man ssh" o "ssh -help".